﻿Public Class RegisterForm

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        For Each tb As TextBox In Me.Controls.OfType(Of TextBox)
            tb.Text = String.Empty
        Next
        txtFristName.Select()
    End Sub

    Private Sub RegisterForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ShowUserData()
        txtFristName.Select()
    End Sub

    'Xuat User
    Private Sub ShowUserData()
        Dim UserData As DataTable = ExecuteSQL("SELECT [UserId],[FirstName]+' '+[LastName] AS FullName,[UserName] FROM [dbo].[User] WHERE [IsDelete] = 0")

        With DataGridView1
            .DataSource = UserData
            .Columns(0).HeaderText = "Full Name"
            .Columns(1).HeaderText = "User Name"
            .Columns(0).Width = 234
            .Columns(1).Width = 234
        End With
    End Sub

    'Xoa User khoi db
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If MessageBox.Show("Do you want to permanently delte the selected record?",
                               "Delete Data : ToDoApp", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) =
                                Windows.Forms.DialogResult.Yes Then
                ExecuteSQL("UPDATE [dbo].[User] SET [IsDelete] = 1 WHERE [UserId] = '" & DataGridView1.CurrentRow.Cells(0).Value & "'")

                ShowUserData()

                MessageBox.Show("The record has been deleted.", "Delete data: Todo App", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim btn As MessageBoxButtons = MessageBoxButtons.OK
        Dim ico As MessageBoxIcon = MessageBoxIcon.Information
        Dim caption As String = "Save Data : iBasskung Tutorial"

        If String.IsNullOrEmpty(txtUserId.Text) Then
            MessageBox.Show("Please enter First Name.", caption, btn, ico)
            txtFristName.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtFristName.Text) Then
            MessageBox.Show("Please enter First Name.", caption, btn, ico)
            txtFristName.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtLastName.Text) Then
            MessageBox.Show("Please enter Last Name.", caption, btn, ico)
            txtLastName.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtUserName.Text) Then
            MessageBox.Show("Please enter Username.", caption, btn, ico)
            txtUserName.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtPassWord.Text) Then
            MessageBox.Show("Please enter password.", caption, btn, ico)
            txtPassWord.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtConfirmPassword.Text) Then
            MessageBox.Show("Please enter Confirmation Password.", caption, btn, ico)
            txtConfirmPassword.Select()
            Return
        End If

        If txtPassWord.Text <> txtConfirmPassword.Text Then
            MessageBox.Show("Your password and confirmation password do not match.", caption, btn, ico)
            txtConfirmPassword.Select()
            Exit Sub
        End If

        Dim CheckDuplicates As DataTable = ExecuteSQL("SELECT Username FROM [User] WHERE Username = '" & txtUserName.Text & "'")

        If CheckDuplicates.Rows.Count > 0 Then
            MessageBox.Show("The Username already exists. Please try another username.", caption, btn, ico)
            txtUserName.Focus()
            Return
        End If

        Dim CheckDuplicatesID As DataTable = ExecuteSQL("SELECT Username FROM [User] WHERE UserId = '" & txtUserId.Text & "'")

        If CheckDuplicatesID.Rows.Count > 0 Then
            MessageBox.Show("The UserId already exists. Please try another userId.", caption, btn, ico)
            txtUserId.Focus()
            Return
        End If

        Dim result As DialogResult

        result = MessageBox.Show("Do you want to save the selected record?", "Save Data : Todo App",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Question)


        If result = DialogResult.Yes Then

            Dim mySQL As String = String.Empty

            mySQL &= "INSERT INTO [User] (UserID, FirstName, LastName, Username, Password, DateCreate, Status, IsDelete) "
            mySQL &= "VALUES ('" & txtUserId.Text & "','" & txtFristName.Text & "','" & txtLastName.Text & "',"
            mySQL &= "'" & txtUserName.Text & "','" & txtPassWord.Text & "', '" & DateTime.Now & "','" & True & "','" & False & "')"

            ExecuteSQL(mySQL)

            'Hien thi khi luu User thanh cong
            MessageBox.Show("The record has been saved successfully.", "Save Data : Todo App", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ShowUserData()

            ' Clear all textboxes
            For Each tb As TextBox In Me.Controls.OfType(Of TextBox)()
                tb.Text = String.Empty
            Next
        Else
            Return
        End If
    End Sub
End Class