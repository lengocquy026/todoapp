﻿Imports System.IO
Imports System.Data

Public Class MainForm
    Private Sub ShowToDoData()
        Dim sql As String = String.Empty
        sql &= "SELECT td.TodoId,td.TodoName,td.Content,td.DateCreate,td.Deadline,DATEDIFF(DAY, GETDATE(),td.Deadline) AS [Date], td.UserId FROM [dbo].[Todo] td "
        sql &= "INNER JOIN [dbo].[User] u ON td.UserID = u.UserID "
        sql &= "WHERE u.UserName = '" & LoginForm.username & "' "
        sql &= "AND td.IsDelete = 0 ORDER BY [Date] ASC"

        Dim UserData As DataTable = ExecuteSQL(sql)

        With dgvTodo
            .DataSource = UserData
            .RowHeadersVisible = False
        End With
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ShowToDoData()
    End Sub

    Private Sub btnCSV_Click(sender As Object, e As EventArgs) Handles btnCSV.Click
        'create empty string
        Dim thecsvfile As String = String.Empty
        'get the column headers
        For Each column As DataGridViewColumn In dgvTodo.Columns
            thecsvfile = thecsvfile & column.HeaderText & ","
        Next
        'trim the last comma
        thecsvfile = thecsvfile.TrimEnd(",")
        'Add the line to the output
        thecsvfile = thecsvfile & vbCr & vbLf
        'get the rows
        For Each row As DataGridViewRow In dgvTodo.Rows
            'get the cells
            For Each cell As DataGridViewCell In row.Cells
                thecsvfile = thecsvfile & cell.FormattedValue.replace(",", "") & ","
            Next
            'trim the last comma
            thecsvfile = thecsvfile.TrimEnd(",")
            'Add the line to the output
            thecsvfile = thecsvfile & vbCr & vbLf
        Next
        'write the file
        My.Computer.FileSystem.WriteAllText("export.csv", thecsvfile, False)
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim btn As MessageBoxButtons = MessageBoxButtons.OK
        Dim ico As MessageBoxIcon = MessageBoxIcon.Information
        Dim caption As String = "Save Data : Todo App"

        If String.IsNullOrEmpty(txtTodoId.Text) Then
            MessageBox.Show("Please enter ToDo ID.", caption, btn, ico)
            txtTodoId.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtTodoName.Text) Then
            MessageBox.Show("Please enter ToDo Name.", caption, btn, ico)
            txtTodoName.Select()
            Return
        End If

        If String.IsNullOrEmpty(txtContent.Text) Then
            MessageBox.Show("Please enter content todo.", caption, btn, ico)
            txtContent.Select()
            Return
        End If

        Dim result As DialogResult

        result = MessageBox.Show("Do you want to save the selected record?", "Save Data : Todo App",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Question)


        If result = DialogResult.Yes Then

            Dim mySQL As String = String.Empty

            mySQL &= "INSERT INTO [dbo].[Todo] ([TodoId],[TodoName],[Content],[DateCreate],[Deadline],[UserID],[IsDelete]) "
            mySQL &= "VALUES ('" & txtTodoId.Text & "','" & txtContent.Text & "','" & txtTodoName.Text & "',"
            mySQL &= "'" & DateTime.Now & "','" & DateTimePicker_Deadline.Value & "', '" & dgvTodo.CurrentRow.Cells(6).Value & "','" & False & "')"

            ExecuteSQL(mySQL)

            'Hien thi khi luu User thanh cong
            MessageBox.Show("The record has been saved successfully.", "Save Data : Todo App", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ShowToDoData()

            ' Clear all textboxes
            For Each tb As TextBox In Me.Controls.OfType(Of TextBox)()
                tb.Text = String.Empty
            Next
        Else
            Return
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If MessageBox.Show("Do you want to permanently delte the selected record?",
                               "Delete Data : ToDoApp", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) =
                                Windows.Forms.DialogResult.Yes Then
                ExecuteSQL("UPDATE [dbo].[Todo] SET [IsDelete] = 1 WHERE [TodoId] = '" & dgvTodo.CurrentRow.Cells(0).Value & "'")

                ShowToDoData()

                MessageBox.Show("The record has been deleted.", "Delete data: Todo App", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try
            If MessageBox.Show("Do you want to permanently update the selected record?",
                               "Update Data : ToDoApp", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) =
                                Windows.Forms.DialogResult.Yes Then
                ExecuteSQL("UPDATE [dbo].[Todo] SET [TodoName] = '" & txtTodoName.Text & "',[Content] = '" & txtContent.Text & "',[Deadline] = '" & DateTimePicker_Deadline.Value & "',[UDDelete] = '" & DateTime.Now & "' WHERE [TodoId] =  '" & txtTodoId.Text & "'")

                ShowToDoData()

                MessageBox.Show("The record has been uupdate.", "Update data: Todo App", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Me.Hide()
        Dim formMain As New LoginForm()
        formMain.ShowDialog()
    End Sub
End Class
