﻿Imports TodoApp.SQLServerConnection

Public Class LoginForm

    Public username As String

    Private Sub LoginForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AcceptButton = btnLogin
        txtUsername.Focus()

        If My.Settings.rememberMe = True Then
            cbRememberMe.Enabled = False
            cbRememberMe.Checked = True
            txtUsername.Text = My.Settings.username
            txtPassword.Text = My.Settings.password
            lbForgetmet.Enabled = True
        Else
            lbForgetmet.Enabled = False
        End If
        txtUsername.Select()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Application.Exit()
    End Sub

    Private Sub OpenRegisterForm_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles OpenRegisterForm.LinkClicked
        RegisterForm.ShowDialog()
    End Sub

    Private Sub cbShowpassword_CheckedChanged(sender As Object, e As EventArgs) Handles cbShowpassword.CheckedChanged
        With cbShowpassword
            If .Checked Then
                txtPassword.UseSystemPasswordChar = False
            Else
                txtPassword.UseSystemPasswordChar = True
            End If
        End With
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Try
            If cbRememberMe.Checked = True Then
                If txtUsername.Text <> Nothing Or txtPassword.Text <> Nothing Then
                    My.Settings.username = txtUsername.Text
                    My.Settings.password = txtPassword.Text
                    My.Settings.rememberMe = True
                    My.Settings.Save()
                    My.Settings.Reload()
                    username = txtUsername.Text

                    Dim sql As String = String.Empty
                    sql &= "SELECT * FROM [User] "
                    sql &= "WHERE UserName = '" & txtUsername.Text & "' "
                    sql &= "AND PassWord = '" & txtPassword.Text & "' "

                    Dim UserData As DataTable = ExecuteSQL(sql)

                    If UserData.Rows.Count > 0 Then
                        txtUsername.Clear()
                        txtPassword.Clear()
                        cbShowpassword.Checked = False

                        Me.Hide()


                        Dim formMain As New MainForm()
                        formMain.ShowDialog()
                        formMain = Nothing

                        Me.txtUsername.Select()
                    Else
                        MessageBox.Show("Username and password are required. Plesa fill the requirement.", "Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        txtUsername.Focus()
                    End If
                Else
                    MessageBox.Show("Username and password are required. Plesa fill the requirement.", "Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtUsername.Focus()
                End If
            Else
                If txtUsername.Text <> Nothing Or txtPassword.Text <> Nothing Then
                    My.Settings.username = txtUsername.Text
                    My.Settings.password = txtPassword.Text
                    My.Settings.rememberMe = True
                    My.Settings.Save()
                    My.Settings.Reload()
                    username = txtUsername.Text

                    Dim sql As String = String.Empty
                    sql &= "SELECT * FROM [User] "
                    sql &= "WHERE UserName = '" & txtUsername.Text & "' "
                    sql &= "AND PassWord = '" & txtPassword.Text & "' "

                    Dim UserData As DataTable = ExecuteSQL(sql)

                    If UserData.Rows.Count > 0 Then
                        txtUsername.Clear()
                        txtPassword.Clear()
                        cbShowpassword.Checked = False

                        Me.Hide()


                        Dim formMain As New MainForm()
                        formMain.ShowDialog()
                        formMain = Nothing

                        Me.Show()

                        Me.txtUsername.Select()
                    Else
                        MessageBox.Show("Username and password are required. Plesa fill the requirement.", "Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        txtUsername.Focus()
                    End If
                Else
                    MessageBox.Show("Username and password are required. Plesa fill the requirement.", "Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtUsername.Focus()
                    txtUsername.Select()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Incorrect setting. Please check your defintion again", "Waring", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

#Region "Login"
        'If Not String.IsNullOrEmpty(txtUsername.Text) And
        '        Not String.IsNullOrEmpty(txtUsername.Text) Then
        '    Dim sql As String = String.Empty
        '    sql &= "SELECT * FROM [User] "
        '    sql &= "WHERE UserName = '" & txtUsername.Text & "' "
        '    sql &= "AND PassWord = '" & txtPassword.Text & "' "

        '    Dim UserData As DataTable = ExecuteSQL(sql)

        '    If UserData.Rows.Count > 0 Then
        '        txtUsername.Clear()
        '        txtPassword.Clear()
        '        cbShowpassword.Checked = False

        '        Me.Hide()

        '        Dim formMain As New MainForm()
        '        formMain.ShowDialog()
        '        formMain = Nothing

        '        Me.Show()
        '        Me.txtUsername.Select()
        '    Else
        '        MsgBox("The User or Password is incorrect. Try again", MsgBoxStyle.Critical, "Login Form: TodoApp")
        '        txtUsername.Focus()
        '        txtUsername.SelectAll()
        '    End If
        'Else
        '    MsgBox("Please enter Username and Password", MsgBoxStyle.Critical, "Login Form: TodoApp")
        '    txtUsername.Select()
        'End If
#End Region

    End Sub

    Private Sub lbForgetmet_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lbForgetmet.LinkClicked
        Try
            Dim ask As String = MessageBox.Show("Are you sure to remove login seesion?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If ask = MsgBoxResult.Yes Then
                My.Settings.Reset()
                cbRememberMe.Enabled = True
                cbRememberMe.Enabled = True
                txtUsername.Text = ""
                txtPassword.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show("Incorrect setting. Please check your defintion again", "Waring", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class